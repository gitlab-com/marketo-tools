# marketo-tools

**Archived**: This project is no longer used, see https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/2331 .


This is a simple Sinatra application that receives several web hooks and
forwards the relevant information to Marketo via its REST API.

## Handlers

### Newsletter

```text
GitLab.com Sign-Up -> System Hook -> marketo-tools -> Marketo
```

Receives the `user_create` system hook from GitLab.com and creates or updates a
lead in Marketo.

This replaces our internal [mailchimp-tools] project.

**Relevant issues:**

- [Point webhook from Mailchimp to Marketo](https://gitlab.com/gitlab-com/www-gitlab-com/issues/427)
- [Remove nl-importer.gitlap.com](https://dev.gitlab.org/cookbooks/chef-repo/issues/316)
  (internal)

## Development

1. Edit the `.env` file to view or change the HTTP Basic Auth credentials and to
   add the Marketo API keys.
1. Start the server:

    ```sh
    bundle install
    bundle exec rackup -p 4567
    ```

## Deployment

This project stores HTTP Basic Auth credentials and Marketo API keys in the
environment variables, using [Dotenv]. The default values should be overridden
in a `.env.production` file for deployment.

The required values are in the [marketo-tools secrets](https://gitlab.1password.com/vaults/medb67sd6uwuzzv66nobz2hjxq/allitems/c7f6rvguqfal5ep4xgol5vjsfa)
login in the **DevOps** vault in 1Password.

See [cookbook-marketo-tools](https://gitlab.com/gitlab-cookbooks/cookbook-marketo-tools)
for more details.

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

Copyright &copy; 2015-2017 GitLab, Inc. It is free software, and may be redistributed
under terms specified in the [`LICENSE`](LICENSE) file.

[mailchimp-tools]: https://dev.gitlab.org/gitlab/mailchimp-tools
[Dotenv]: https://github.com/bkeepers/dotenv

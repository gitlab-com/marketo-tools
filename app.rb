require 'oj'

require './lib/marketo_client'
require './lib/user'

class MarketoTools < Sinatra::Base
  use Rack::Auth::Basic do |username, password|
    username == ENV['AUTH_USERNAME'] && password == ENV['AUTH_PASSWORD']
  end

  get '/' do
    'success'
  end

  # Receives GitLab System Hooks
  #
  # Specifically, we're interested in the `user_create` event.
  #
  # See http://gitlab.com/help/system_hooks/system_hooks
  post '/system_hook' do
    begin
      params = Oj.load(request.body.read)
    rescue Oj::ParseError
      params = {}
    end

    if params && params['event_name'] == 'user_create'
      user = User.new(params)

      client.createupdate_leads([user.to_param], lookup_field: :email)
    end

    # https://dev.gitlab.org/gitlab/mailchimp-tools/commit/a93a0b97a906008e97c0fe6eccdf09ce32f944ea
    'success'
  end

  private

  def client
    @client ||= MarketoClient.new
  end
end

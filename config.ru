require 'bundler'

Bundler.require
Dotenv.load(".env.#{ENV['RACK_ENV']}", '.env')

if ENV['RAVEN_DSN']
  Raven.configure do |config|
    config.dsn = ENV['RAVEN_DSN']
  end

  use Raven::Rack
end

require './app'
run MarketoTools

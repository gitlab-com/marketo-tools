class MarketoClient < SimpleDelegator
  attr_reader :client

  def initialize
    @client = Mrkt::Client.new(
      host:          ENV['MRKT_HOST'],
      client_id:     ENV['MRKT_ID'],
      client_secret: ENV['MRKT_SECRET']
    )

    super(@client)
  end
end

class User
  include FullNameSplitter

  attr_accessor :first_name, :last_name, :email, :email_opted_in,
                :email_opted_in_ip, :email_opted_in_source, :email_opted_in_at

  def initialize(params)
    self.full_name             = params['name']
    self.email                 = params['email']
    self.email_opted_in        = params['email_opted_in']
    self.email_opted_in_ip     = params['email_opted_in_ip']
    self.email_opted_in_source = params['email_opted_in_source']
    self.email_opted_in_at     = params['email_opted_in_at']
  end

  def to_param
    {
      email:      email,
      firstName:  first_name,
      lastName:   last_name,
      signUpDate: Date.today,
      leadSource: email_opted_in_source,
      optedIn: email_opted_in,
      optedInIp: email_opted_in_ip,
      optedInAt: email_opted_in_at,
      GitLab_com_user__c: true,
      GitLab_com_user__c_account: true
    }
  end
end
